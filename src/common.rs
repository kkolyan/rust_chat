pub trait MyVec<T> {
    fn single(&self) -> Result<T, String>;
}

impl<T: Clone> MyVec<T> for Vec<T> {
    fn single(&self) -> Result<T, String> {
        if self.len() == 1 {
            Ok(self.get(0).unwrap().clone())
        } else {
            Err("cannot resolve single value".to_string())
        }
    }
}