use std::net::{SocketAddr, ToSocketAddrs};
use std::thread::spawn;
use crate::common::MyVec;

mod args;
mod common;
mod cli_client;
mod server;

fn main() {
    let args = args::parse_args();

    let address = args.get("--address")
        .expect("--address required")
        .single().expect("--remote_host should provide single value");

    let is_host = args.get("--host").is_some();
    let is_dedicated = args.get("--dedicated").is_some();

    let addrs = address.to_socket_addrs().expect("failed to resolve host")
        .collect::<Vec<SocketAddr>>();
    let addr = addrs.first().expect("invalid host").clone();

    if is_host {
        let mut instance = server::ChatInstance::bind(addr.clone()).expect("failed to bind");
        if !is_dedicated {
            spawn(move || {
                cli_client::run_client(addr.clone());
            });
        }
        instance.run_accept_loop();
    } else {
        println!("connecting to {}", address);

        cli_client::run_client(addr.clone());
    }
}