use std::io::{BufRead, BufReader, stdin, Write};
use std::net::{SocketAddr, TcpStream};
use std::process::exit;
use std::thread;
use thread::spawn;

pub fn run_client(addr: SocketAddr) {
    let mut connection = TcpStream::connect(addr)
        .expect("failed to connect");

    println!("connected to {}", addr);

    let mut inbound_stream = BufReader::new(connection.try_clone().unwrap());
    spawn(move || {
        let mut buf = String::new();
        loop {
            if let Ok(line_len) = inbound_stream.read_line(&mut buf) {
                println!("{}", &buf[..line_len - 1]);
                buf.clear();
            } else {
                println!("[HANDLER] disconnected");
                break;
            }
        }
    });
    let mut buf = String::new();
    loop {
        if let Ok(line_len) = stdin().read_line(&mut buf) {
            let success = connection.write(buf[..line_len - 1].as_bytes()).is_ok()
                && connection.write("\n".as_bytes()).is_ok()
                && connection.flush().is_ok();
            buf.clear();

            if !success {
                exit(0);
            }
        }
    }
}