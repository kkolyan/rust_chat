use std::collections::HashMap;
use std::env;

pub fn parse_args() -> HashMap<String, Vec<String>> {
    let mut results: HashMap<String, Vec<String>> = HashMap::new();
    let args: Vec<String> = env::args().collect();
    let mut key = "".to_string();
    for arg in args {
        if arg.starts_with("--") {
            key = arg;
            results.entry(key.clone()).or_default();
        } else {
            results.entry(key.clone()).or_default().push(arg);
        }
    }
    results
}

trait Arg {
    fn as_string(&self, key: &str) -> Result<String, String>;
    fn as_int(&self, key: &str) -> Result<i32, String>;
}

impl Arg for HashMap<String, Vec<String>> {
    fn as_string(&self, key: &str) -> Result<String, String> {
        match self.get(key) {
            None => Err(format!("arg not found: {}", key)),
            Some(values) => {
                if values.len() != 1 {
                    Err(format!("arg has no unique value: {} (given: {})", key, values.join(", ")))
                } else {
                    Ok(values.get(0).unwrap().to_string())
                }
            }
        }
    }

    fn as_int(&self, key: &str) -> Result<i32, String> {
        let result = self.as_string(key)?;
        match result.parse() {
            Ok(v) => Ok(v),
            Err(err) => Err(err.to_string())
        }
    }
}