use std::cell::RefCell;
use std::collections::{HashMap, VecDeque};
use std::io::{LineWriter, Error, BufReader, BufRead, Write};
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::net::Shutdown::Both;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicI64, Ordering};
use std::thread::spawn;

pub struct ChatInstance {
    listener: TcpListener,
}

struct Session {
    name: String,
    addr: SocketAddr,
    outbound_stream: LineWriter<TcpStream>,
}

impl ChatInstance {
    pub fn bind(addr: SocketAddr) -> Result<ChatInstance, Error> {
        let listener = TcpListener::bind(addr)?;
        println!("bound to {}", addr);
        Ok(ChatInstance { listener })
    }

    pub fn run_accept_loop(&mut self) {
        let sessions: Arc<Mutex<RefCell<HashMap<i64, Session>>>> = Default::default();
        let next_connection_id: Arc<AtomicI64> = Default::default();
        loop {
            println!("[ACCEPTOR] accepting...");
            let (stream, addr) = self.listener.accept().expect("failed to accept");
            println!("[ACCEPTOR] connection accepted: {}", addr);

            let sessions = sessions.clone();
            let next_connection_id = next_connection_id.clone();
            spawn(move || {
                let mut inbound_stream = BufReader::new(&stream);
                let mut buf = String::new();

                let mut outbound_stream = LineWriter::new(stream.try_clone()
                    .expect("failed to clone brand new stream"));

                let _ = outbound_stream.write("welcome to the chat! enter your name.\n".as_bytes());

                let session_name: String;
                if let Ok(line_len) = inbound_stream.read_line(&mut buf) {
                    session_name = buf[..line_len - 1].to_string();
                    buf.clear();
                } else {
                    println!("[WORKER] disconnecting: {}", addr);
                    let _ = stream.shutdown(Both);
                    return;
                }


                let session_id = next_connection_id.fetch_add(1, Ordering::Relaxed);
                let session = Session {
                    name: session_name.clone(),
                    addr: addr.clone(),
                    outbound_stream,
                };
                {
                    let mut guard = sessions.lock().unwrap();
                    guard.get_mut().insert(session_id, session);
                }

                let mut disconnect_queue: VecDeque<i64> = Default::default();
                let mut broadcast_queue: VecDeque<String> = Default::default();

                broadcast_queue.push_back(format!("{} joined chat", session_name));

                loop {
                    while !broadcast_queue.is_empty() {
                        while let Some(message) = broadcast_queue.pop_front() {
                            let mut sessions = sessions.lock().unwrap();
                            for (id, session) in sessions.get_mut().iter_mut() {
                                let session = &mut session.outbound_stream;
                                let success = session.write(message.as_bytes()).is_ok()
                                    && session.write("\n".as_bytes()).is_ok()
                                    && session.flush().is_ok();
                                if !success {
                                    disconnect_queue.push_back(*id);
                                }
                            }
                            while let Some(id) = disconnect_queue.pop_front() {
                                if let Some(session) = sessions.get_mut().remove(&id) {
                                    println!("[WORKER] disconnected: {}", session.addr);
                                    broadcast_queue.push_back(format!("left: {}", session.name));
                                }
                            }
                        };
                    }
                    if let Ok(line_len) = inbound_stream.read_line(&mut buf) {
                        broadcast_queue.push_back(format!("{}: {}", session_name, &buf[..line_len - 1]));
                        buf.clear();
                    } else {
                        sessions.lock().unwrap().get_mut().remove(&session_id);
                        broadcast_queue.push_back(format!("{} left chat", session_name));
                    }
                }
            });
        }
    }
}

